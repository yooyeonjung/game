#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <shader_s.h>
#include <mesh.h>

#include <vector>

class Light
{
public:
	glm::vec3 Position;
	Shader_s Shader;
	//Mesh LightCube(vertices);
	Light();
	Light(glm::vec3 position, Shader_s shader);
private:
	// Calculates the front vector from the Camera's (updated) Euler Angles
	
};
