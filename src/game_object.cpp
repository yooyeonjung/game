#include <game_object.h>

GameObject::GameObject() {}

GameObject::GameObject(glm::vec3 pos, glm::vec3 size, const char *file_path, Shader_s shader, Object_Type type, glm::vec3 color = glm::vec3(1.0f), glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f))
{
	Position = pos;
	Size = size;
	Velocity = velocity;
	Color = color;
	Shader = shader;
	if (type == OBJECT_3D)
		model_o = Model(file_path, false);
}