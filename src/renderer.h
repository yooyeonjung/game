#pragma once

#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <game_object.h>
#include <camera.h>
#include <shader_s.h>

#include <vector>

class Renderer
{
public:
	GLFWwindow *Window;
	std::vector<GameObject> Objects;
	Camera camera;
	Shader_s Shader;

	glm::mat4 projection;
	glm::mat4 view;

	int Width, Height;
	Renderer();
	Renderer(GLFWwindow *window, Camera camera_a);

	Renderer(GLFWwindow *window, Camera camera_a, std::vector<GameObject> objects);

	void clearScreen();

	void Render();

	void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);
	void ProcessKeyboard(Camera_Movement direction, float deltaTime);
	void ProcessMouseScroll(float yoffset);

	void getRayCast();

private:
	void Initialize();
	void DrawModel(GameObject object);
};
