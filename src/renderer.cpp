#include <renderer.h>

Renderer::Renderer(){}

Renderer::Renderer(GLFWwindow *window, Camera camera_a)
{
	Window = window;
	camera = camera_a;
	Initialize();
}

Renderer::Renderer(GLFWwindow *window, Camera camera_a, std::vector<GameObject> objects)
{
	Window = window;
	Objects = objects;
	camera = camera_a;
	Initialize();
}

void Renderer::clearScreen() {
	glClearColor(0.1f, 0.1f, 0.1f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void Renderer::Render() {
	clearScreen();
	for (unsigned int i = 0; i < Objects.size(); i++)
	{
		Shader = Objects[i].Shader;
		Shader.use();
		projection = glm::perspective(glm::radians(camera.Zoom), (float)Width / (float)Height, 0.1f, 100.0f);
		Shader.setMat4("projection", projection);
		view = camera.GetViewMatrix();
		Shader.setMat4("view", view);
		// render the loaded model
		glm::mat4 model;
		//model = glm::translate(model, object.Position); // translate it down so it's at the center of the scene
		model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
		//model = glm::scale(model, object.Size);	// it's a bit too big for our scene, so scale it down
		model = glm::scale(model, glm::vec3(0.02f, 0.02f, 0.02f));	// it's a bit too big for our scene, so scale it down
		Objects[i].Shader.setMat4("model", model);
		Objects[i].model_o.Draw(Objects[i].Shader);
		printf("processing obj number: %d\n", i);
		//DrawModel(Objects[i]);
	}
	return;
}

void Renderer::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
	camera.ProcessMouseMovement(xoffset, yoffset, constrainPitch);
}
void Renderer::ProcessKeyboard(Camera_Movement direction, float deltaTime)
{
	camera.ProcessKeyboard(direction, deltaTime);
}
void Renderer::ProcessMouseScroll(float yoffset)
{
	camera.ProcessMouseScroll(yoffset);
}

void Renderer::getRayCast()
{
	double mouse_x, mouse_y;
	glfwGetCursorPos(Window, &mouse_x, &mouse_y);
	float x = (2.0f * (float)mouse_x) / Width - 1.0f;
	float y = 1.0f - (2.0f * (float)mouse_y) / Height;
	float z = 1.0f;

	glm::vec3 ray_nds = glm::vec3(x, y, z);
	glm::vec4 ray_clip = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 1.0);
	glm::vec4 ray_eye = glm::inverse(projection) * ray_clip;
	ray_eye = glm::vec4(ray_nds.x, ray_nds.y, -1.0, 0.0);

	glm::vec3 temp_vec = glm::inverse(view) * ray_eye;
	glm::vec3 ray_wor = glm::vec3(temp_vec.x, temp_vec.y, temp_vec.z);
	ray_wor = glm::normalize(ray_wor);
	printf("%f, %f, %f\n", ray_wor.x, ray_wor.y, ray_wor.z);
}

void Renderer::Initialize() {
	glEnable(GL_DEPTH_TEST);
	glfwGetWindowSize(Window, &Width, &Height);
}

void Renderer::DrawModel(GameObject object)
{
	// render the loaded model
	glm::mat4 model;
	//model = glm::translate(model, object.Position); // translate it down so it's at the center of the scene
	model = glm::translate(model, glm::vec3(0.0f, -1.75f, 0.0f)); // translate it down so it's at the center of the scene
	//model = glm::scale(model, object.Size);	// it's a bit too big for our scene, so scale it down
	model = glm::scale(model, glm::vec3(0.2f, 0.2f, 0.2f));	// it's a bit too big for our scene, so scale it down
	object.Shader.setMat4("model", model);
	object.model_o.Draw(object.Shader);
}