#pragma once

#include <glad/glad.h>
#include <glm/glm.hpp>

#include "model.h"

#include <texture.h>

// Defines several possible options for camera movement. Used as abstraction to stay away from window-system specific input methods
enum Object_Movement {
	OBJECT_FORWARD,
	OBJECT_BACKWARD,
	OBJECT_LEFT,
	OBJECT_RIGHT
};

enum Object_Type {
	OBJECT_2D,
	OBJECT_3D
};

// Container object for holding all state relevant for a single
// game object entity. Each object in the game likely needs the
// minimal of state as described within GameObject.
class GameObject
{
public:
	// Object state
	glm::vec3   Position, Size, Velocity;
	glm::vec3   Color;
	Model model_o;
	Shader_s Shader;
	GLfloat     Rotation;
	GLboolean   IsSolid;
	GLboolean   Destroyed;
	float MovementSpeed = 1.0f;
	//Constructor(s)
	GameObject();
	GameObject(glm::vec3 pos, glm::vec3 size, const char *file_path, Shader_s shader, Object_Type type, glm::vec3 color = glm::vec3(1.0f), glm::vec3 velocity = glm::vec3(0.0f, 0.0f, 0.0f));

};
